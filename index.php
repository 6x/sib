<?php
global $mysqli;
$servername = "localhost";
$username1 = "root";
$password = "root";
$dbname = "mydb";

// Create connection
$conn = new mysqli($servername, $username1, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$query = "SELECT title, text, id FROM text_table";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    
<link rel="icon" type="image/png" href="images/favicon.png" />
    <title>SIB</title>
</head>
<body>
<div class="container">
<div class="write">
  Send:
<form action="input.php" method="post">
 <p>title: <input type="text" name="title" required /></p>
 <p>text: <textarea type="text" name="text" required>
 </textarea>
 </p>
 <p><input type="submit" /></p>
  </div>
  <div class="content">
  <?
 if ($result = mysqli_query($conn, $query)) {
/* выборка данных и помещение их в массив */
while ($row = mysqli_fetch_row($result)) {
    echo "<b style='color:#54565B;'>$row[0]</b><b style='color:#76777B; float:right;'>№ $row[2]</b><br><hr noshade color='black' size='1px'> <p>$row[1]</p></br>";
}
/* очищаем результирующий набор */
mysqli_free_result($result);
}
mysqli_close($conn);
?>
  </div>
  <div class="navbar">
  
  </div>
</div>


</form>
</body>
</html>
